package com.BookApp.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.BookApp.Model.Book;
import com.BookApp.exception.IdNotFoundExcepton;
import com.BookApp.exception.bookNotFoundException;
import com.BookApp.repository.DbManager;
import com.BookApp.repository.IBookRepo;
import com.BookApp.repository.IBookRepoImpl;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;

public class IBookServiceImpl implements IBookService {
    IBookRepo bookrepo=new IBookRepoImpl();
	public List<Book> getAll() {
		return bookrepo.findAll();
	}

	public List<Book> getByAuthor(String author) throws bookNotFoundException {
		// TODO Auto-generated method stub
		List<Book> book=bookrepo.findByAuthor(author);
		if(!book.isEmpty())
			Collections.sort(book,(b1,b2)->b1.getTitle().compareTo(b2.getTitle()));
		
		if(book.isEmpty())
			throw new bookNotFoundException("book not found");
			else return book;		
	}

	@Override
	public List<Book> getByCategory(String category) throws bookNotFoundException {
		// TODO Auto-generated method stub
		List<Book> book=bookrepo.findByCategory(category);
		if(!book.isEmpty())
			Collections.sort(book,(b1,b2)->b1.getTitle().compareTo(b2.getTitle()));
		if(book.isEmpty())
			throw new bookNotFoundException("book not found");
			else return book;	}

	@Override
	public Book getById(int bookId) throws IdNotFoundExcepton {
		// TODO Auto-generated method stub
		Book book=bookrepo.findById(bookId);
		if(book!=null)
			return book;
		else throw new IdNotFoundExcepton("Id Not Found");
	}

	@Override
	public List<Book> getByLesserPrice(double price) throws bookNotFoundException {
		// TODO Auto-generated method stub
		List<Book> book=bookrepo.findByLesserPrice(price);
		if(!book.isEmpty())
			Collections.sort(book,(b1,b2)->b1.getTitle().compareTo(b2.getTitle()));
		if(book.isEmpty())
		throw new bookNotFoundException("book not found");
		else return book;
	}

	
	public void addBook(Book book) {
		// TODO Auto-generated method stub
		bookrepo.addBook(book);
	}

	@Override
	public void updateBook(String author, double price) throws bookNotFoundException{
		// TODO Auto-generated method stub
		MongoCollection<Book> collection=DbManager.getCollection();
		List<Book> book=collection.find(Filters.eq("author", author)).into(new ArrayList<>());
		if(book.isEmpty())
			throw new bookNotFoundException("No Book found to update");
		bookrepo.updateBook(author, price);
	}

	@Override
	public void deleteBook(String author) throws bookNotFoundException{
		// TODO Auto-generated method stub
		MongoCollection<Book> collection=DbManager.getCollection();
		List<Book> book=collection.find(Filters.eq("author",author)).into(new ArrayList<>());
		if(book.isEmpty())
			 throw new bookNotFoundException("No book is there for deletion");		
		bookrepo.deleteBook(author);
    }
	public void addManyBook(List<Book> book) {
		bookrepo.addManyBook(book);
	}
	

	
}
