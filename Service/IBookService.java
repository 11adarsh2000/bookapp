package com.BookApp.Service;
import com.BookApp.Model.Book;
import com.BookApp.exception.IdNotFoundExcepton;
import com.BookApp.exception.bookNotFoundException;

import java.util.*;
public interface IBookService {
      void addBook(Book book);
      void updateBook(String author,double price)throws bookNotFoundException;
      void deleteBook(String author)throws bookNotFoundException;
	
	
	   List<Book> getAll();
       List<Book> getByAuthor(String author) throws  bookNotFoundException;
       List<Book> getByCategory(String category) throws bookNotFoundException;
       Book getById(int bookId) throws IdNotFoundExcepton;
       List<Book> getByLesserPrice(double price) throws bookNotFoundException;
	    void addManyBook(List<Book> book);

}
