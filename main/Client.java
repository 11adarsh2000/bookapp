package com.BookApp.main;

import com.BookApp.Model.Book;
import com.BookApp.Service.IBookService;
import com.BookApp.Service.IBookServiceImpl;
import com.BookApp.exception.IdNotFoundExcepton;
import com.BookApp.exception.bookNotFoundException;
import com.BookApp.repository.DbManager;

import java.util.Arrays;
import java.util.List;

public class Client {
      public static void main(String[] args) throws Exception{
    	DbManager.openConnection();

        IBookService client=new IBookServiceImpl();
        client.addManyBook(Arrays.asList(
				new Book("Java In Action",1,"Morris",234.2,"Programming"),
				new Book("Introduction to C",2,"Kanetkar",223,"Programming"),
				new Book("Dark Nights",3,"Hellen",123,"Novel"),
				new Book("BookIn",4,"Raghu",654.2,"Novel"),
				new Book("EDC",5,"erley",500,"Descriptive"),
				new Book("Control",6,"Morris",2342,"Descriptive"),
				new Book("EKT",7,"Morris",231,"Descriptive"),
				new Book("Java",8,"Morris",34.2,"Descriptive")

				));
		try {
           client.deleteBook("Heln");
		}catch(bookNotFoundException e) {
			e.getMessage();
		}
		try {
		client.updateBook("Kanetar", 98);
		}catch(bookNotFoundException e) {
			e.getMessage();
		}
		
			System.out.println("All the books");
			List<Book> listOfAll=client.getAll();
			for(Book b:listOfAll) {
				System.out.println(b);
			}
			
		try {	
			System.out.println("All the books with Author");
			List<Book> listOfBookByAuthor=client.getByAuthor("Morris");
			for(Book b:listOfBookByAuthor) {
				System.out.println(b);
			}
		}catch(bookNotFoundException e) {
			e.getMessage();
		}
		try {	
			System.out.println("All the books with Category");
			List<Book> listOfBookByCategory=client.getByCategory("Programming");
			for(Book b:listOfBookByCategory) {
				System.out.println(b);
			}
		}catch(bookNotFoundException e) {
			e.getMessage();
		}
		try {
		    System.out.println("All the books with Price");
			List<Book> listOfBookByLesserPrice=client.getByLesserPrice(1000);
			for(Book b:listOfBookByLesserPrice) {
				System.out.println(b);
			}
		}catch(bookNotFoundException e) {
			e.getMessage();
		}
		try {
			System.out.println("All the books with Id");
			Book BookById=client.getById(8);
				System.out.println(BookById);
			}catch(IdNotFoundExcepton e) {
			e.getMessage();
		}
	}
}
