package com.BookApp.Model;

import org.bson.codecs.pojo.annotations.BsonProperty;

public class Book {
        private String title;
        @BsonProperty(value="_id")
        private Integer Id;
        private String author;
        private double price;
        private String category;
		public Book() {
			super();
			// TODO Auto-generated constructor stub
		}
		public Book(String title, Integer bookId, String author, double price, String category) {
			super();
			this.title = title;
			this.Id = bookId;
			this.author = author;
			this.price = price;
			this.category = category;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public Integer getBookId() {
			return Id;
		}
		public void setBookId(Integer bookId) {
			this.Id = bookId;
		}
		public String getAuthor() {
			return author;
		}
		public void setAuthor(String author) {
			this.author = author;
		}
		public double getPrice() {
			return price;
		}
		public void setPrice(double price) {
			this.price = price;
		}
		public String getCategory() {
			return category;
		}
		public void setCategory(String category) {
			this.category = category;
		}
		@Override
		public String toString() {
			return "Book [title=" + title + ", Id=" +Id + ", author=" + author + ", price=" + price
					+ ", category=" + category + "]";
		}
		
		
}
