package com.BookApp.exception;

public class bookNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public bookNotFoundException() {
		super();
	}

	public bookNotFoundException(String message) {
		super(message);
        System.out.println(message);
	}

}
