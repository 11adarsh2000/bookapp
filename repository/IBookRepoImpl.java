package com.BookApp.repository;

import java.util.*;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.BookApp.Model.Book;
import com.BookApp.exception.IdNotFoundExcepton;
import com.BookApp.exception.bookNotFoundException;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import com.sun.net.httpserver.Filter;

public class IBookRepoImpl implements IBookRepo {

	public List<Book> findAll(){
		MongoCollection<Book> collections= DbManager.getCollection();
	    List<Book> list=collections.find().into(new ArrayList<>());
		return list;
	    }

	
	
	public void addBook(Book book) {
     	MongoCollection<Book> collection=DbManager.getCollection();
		collection.insertOne(book);
	}
	public void addManyBook(List<Book> book) {
		MongoCollection<Book> collection=DbManager.getCollection();
		collection.insertMany(book);
	}

	@Override
	public void updateBook(String author, double price) throws bookNotFoundException{
		MongoCollection<Book> collection=DbManager.getCollection();
		List<Book> book=collection.find(Filters.eq("author", author)).into(new ArrayList<>());
		if(book.isEmpty())
			throw new bookNotFoundException("No Book found to update");
        collection.updateOne(Filters.eq("author",author),Updates.set("price",price));		
	}

	@Override
	public void deleteBook(String author) throws bookNotFoundException{
		MongoCollection<Book> collection=DbManager.getCollection();
		List<Book> book=collection.find(Filters.eq("author",author)).into(new ArrayList<>());
		if(book.isEmpty())
			 throw new bookNotFoundException("No book is there for deletion");
        collection.deleteOne(Filters.eq("author",author));
	}

	@Override
	public List<Book> findByAuthor(String author) throws bookNotFoundException {
		MongoCollection<Book> collections= DbManager.getCollection();
	    List<Book> list=collections.find(Filters.eq("author", author)).into(new ArrayList<>());
	    if(list.isEmpty())
			throw new bookNotFoundException("Not Found Books with Author");
		return list;
	    
	}

	@Override
	public List<Book> findByCategory(String category) throws bookNotFoundException {
		MongoCollection<Book> collections= DbManager.getCollection();
		List<Book> list=collections.find(Filters.eq("category", category)).into(new ArrayList<>());
		if(list.isEmpty())
			throw new bookNotFoundException("Not Found Books with Category");
		return list;	
	    }

	@Override
	public Book findById(int bookId) throws IdNotFoundExcepton {
		MongoCollection<Book> collections= DbManager.getCollection();
	    Document doc=new Document("_id",bookId);
	    Book book=collections.find(doc).first();
	    if(book==null)
			throw new IdNotFoundExcepton("Not Found Books with this Id");
		return book;	}

	@Override
	public List<Book> findByLesserPrice(double price) throws bookNotFoundException {
		MongoCollection<Book> collections= DbManager.getCollection();
	    List<Book> list=collections.find(Filters.lte("price", price)).into(new ArrayList<>());
	    if(list.isEmpty())
			throw new bookNotFoundException("Not Found Books with more lesser price");
		return list;
	}

}
