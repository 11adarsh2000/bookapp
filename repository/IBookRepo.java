package com.BookApp.repository;
import com.BookApp.Model.Book;
import com.BookApp.exception.IdNotFoundExcepton;
import com.BookApp.exception.bookNotFoundException;

import java.util.*;
public interface IBookRepo {
	void addBook(Book book);
    void updateBook(String author,double price) throws  bookNotFoundException;
    void deleteBook(String author) throws  bookNotFoundException;   
	
       List<Book> findAll();
       List<Book> findByAuthor(String author) throws  bookNotFoundException;
       List<Book> findByCategory(String category) throws bookNotFoundException;
       Book findById(int bookId) throws IdNotFoundExcepton;
       List<Book> findByLesserPrice(double price) throws bookNotFoundException;
	    void addManyBook(List<Book> book);
}
